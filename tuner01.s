include "64cube.inc"

ENUM $0

  counter db 1
  timer   db 1
  deduct  db 1
  second  db 1
  static  db 1
  flipper db 1
  src_l   db 1
  src_h   db 1
  dst_l   db 1
  dst_h   db 1
  wide    db 1
  high    db 1
  mode    db 1
  row     db 1
  move    db 1
  ink     db 1
  colour  db 1
  long    db 1
  tomove  db 1
  held    db 1
  last    db 1
  wipe    db 1
  gap     db 1
  count   db 1
  savex   db 1
  key     db 1
  glyph   db 1
  offset  db 1
  index   db 1
  flash   db 1
  delay   db 1
  mask    db 1
  temp    db 1
  ready   db 1
ENDE

  KEYA      EQU  %00000001
  KEYB      EQU  %00000010
  KEYSELECT EQU  %00000100
  KEYSTART  EQU  %00001000
  KEYUP     EQU  %00010000
  KEYDN     EQU  %00100000
  KEYLT     EQU  %01000000
  KEYRT     EQU  %10000000


  org $200
Boot:
  sei
  ldx #$ff
  txs

  _setb #$c,VIDEO
  _setb #$9,COLORS

  _setb #63,long

  lda count
  beq +
  byte $2c
+
  lda #3
  sta count

  _setw IRQ, VBLANK_IRQ
  cli








Main:

  lda ready
  beq Main

    lda mode
    bne +
    jsr Tuner
    +
    cmp #1
    bne +
    jsr Game
    +
    cmp #2
    bne +
    jsr Deny
    +
    cmp #3
    bne +
    jsr Complete
    +

  lda #0
  sta ready

  jmp Main








IRQ:
  lda #1
  sta ready

  lda mode
  bne +

  lda flipper
  cmp #2
  bne ++
  inc static

  lda static
  cmp #8
  bne +++
  lda #0
  sta static
+++
  lda #0
  sta flipper
++
  inc flipper

  rti
+
  cmp #1
  bne +

  lda counter
  cmp #40
  bne ++
  inc deduct

  lda flash
  eor #%00000001
  sta flash

  lda long
  clc
  sbc #1
  sta long

  lda deduct
  cmp #31
  bne +++
  lda #0
  sta deduct
  lda #63
  sta long
  lda #2
  sta mode

+++
  lda #0
  sta counter

++
  inc counter

  lda timer
  cmp #60
  bne ++
  inc second

  lda #0
  sta timer

++
  inc timer


+
  cmp #2
  bne +

  lda delay
  cmp #180
  bne ++

  lda #0
  sta mode
  sta wipe
  sta index
  sta deduct
  sta second
  sta count
  sta row
  sta move
  sta delay

  _setb 56,wide
  _setb 12,high
  _setw $d204,dst_l
  _setb 0,colour
  jsr Block

  jmp Boot

++
  inc delay



  ; jsr Deny
+
  cmp #3
  bne +++

  lda delay
  cmp #180
  bne ++

  lda deduct
  cmp #16
  bcs +

  lda count
  cmp #5
  beq +

  inc count
+
  lda #0
  sta mode
  sta wipe
  sta index
  sta deduct
  sta second
  sta row
  sta move
  sta delay

  jmp Boot

++
  inc delay

+++

  rti








Tuner:

  jsr Clear

  lda INPUT
  cmp #KEYUP
  bne +
  _setb 16,wide
  _setb 3,high
  _setw $c018,dst_l
  jmp ++
+
  cmp #KEYDN
  bne +
  _setb 16,wide
  _setb 3,high
  _setw $cf58,dst_l
  jmp ++
+
  cmp #KEYLT
  bne +
  _setb 3,wide
  _setb 16,high
  _setw $c600,dst_l
  ; _setw $c580,dst_l
  jmp ++
+
  cmp #KEYRT
  bne +
  _setb 3,wide
  _setb 16,high
  _setw $c63d,dst_l
  jmp ++
+
  and #%11110000
  beq +++

  lda #1
  sta mode
  rts
++
  _setb 2,colour
  jsr Block
+++

  _setb 0,mask
  _setb 56,wide
  _setb 56,high

  ldx static

  lda static_l,x
  sta src_l
  lda static_h,x
  sta src_l+1

  lda #1
  sta colour
  _setw $c104,dst_l
  ; jsr Block
  jsr Draw

  _setb 35,wide
  _setb 8,high
  _setw signal,src_l
  _setw $c70e,dst_l


  lda INPUT
  and #%00001111
  bne +
  lda #0
  byte $2c

  ; _setb #0,colour

+
  lda #3
  sta colour
  jsr Block
  _setb 1,colour
_setw $c70e,dst_l
  jsr NoSig

  rts








Game:

  lda #$d
  sta VIDEO

  lda #$a
  sta COLORS

  jsr Controls


  ; CLEAR SELECTION BOX
  _setb 63,wide
  _setb 3,high
  _setb 0,colour
  _setw $d800,dst_l
  jsr Block
  _setw $db80,dst_l
  jsr Block
  _setw $df40,dst_l
  jsr Block

  _setb 6,wide
  _setb 31,high
  _setw $d80d,dst_l
  jsr Block
  _setw $d81d,dst_l
  jsr Block
  _setw $d82d,dst_l
  jsr Block
  _setw $d83d,dst_l
  jsr Block


  lda long
  cmp #20
  bcc +
  lda #0
  byte $2c
+

  lda flash
  beq ++

  lda #5
  byte $2c
++
  lda #0
  sta colour


  ; ALERT BARS
  _setb 17,wide
  _setb 4,high
  _setw $d6c1,dst_l
  jsr Block

  _setw $d6ee,dst_l
  jsr Block

  _setb 7,wide
  _setb 4,high
  _setb 0,colour
  _setw $d6dc,dst_l
  jsr Block


  _setb 0,mask

  ldx second

  lda digit_l,x
  sta src_l
  lda digit_h,x
  sta src_l+1

  lda #1
  sta colour
  _setw $d6dc,dst_l
  jsr Draw


  lda count
  cmp #3
  bne +
  ldx #$54
  jmp ++
+
  cmp #4
  bne +
  ldx #$50
  jmp ++
+
  ldx #$4c
++
  stx offset


  ; DRAW GLYPHS
  lda #2
  sta colour
  ;
  _setb 8,wide
  _setb 10,high

  ldx #0
  stx gap
glyphloop:
  stx savex

  jsr List

  sta glyph
  jsr Vector
  _setw $d200,dst_l
  _addwb dst_l,offset,dst_l
  _addwb dst_l,gap,dst_l
  jsr Draw

  ldx savex

  inx

  lda gap
  clc
  adc #8
  sta gap

  cpx count
  bne glyphloop

  lda wipe
  beq +

  sta wipe
  sta wide

  _setb 10,high
  _setb 0,colour
  _setw $d200,dst_l
  _addwb dst_l,offset,dst_l
  jsr Block
+




  ldx #0
  txa
-
  sta $d601,x
  sta $d641,x

  inx
  cpx #62
  bne -




  lda #4
  sta colour

  ldx long
  cpx #40
  bcc +

  lda #3
  sta colour

+
  cpx #20
  bcs +

  lda #5
  sta colour

+
  lda #$d6
  sta dst_h
  lda deduct
  sta dst_l

  ldy #1
-
  lda colour
  sta (dst_l),y

  iny
  cpy long
  bne -


  lda #$d6
  sta dst_h
  lda deduct
  adc #63
  sta dst_l

  ldy #1
-
  lda colour
  sta (dst_l),y

  iny
  cpy long
  bne -


  _setb 14,wide
  _setb 16,high

  _setw box,src_l
  lda row
  bne +

  _setw $d801,dst_l
  jmp ++
+
  _setw $dbc1,dst_l

++
  lda dst_l
  clc
  adc move
  sta dst_l

  lda ink
  sta colour
  jsr Draw




Dots:
  lda #1
  sta $d800+1
  sta $d800+14
  sta $d800+17
  sta $d800+30
  sta $d800+33
  sta $d800+46
  sta $d800+49
  sta $d800+62

  sta $dbc0+1
  sta $dbc0+14
  sta $dbc0+17
  sta $dbc0+30
  sta $dbc0+33
  sta $dbc0+46
  sta $dbc0+49
  sta $dbc0+62

  sta $df80+1
  sta $df80+14
  sta $df80+17
  sta $df80+30
  sta $df80+33
  sta $df80+46
  sta $df80+49
  sta $df80+62
  rts




Deny:
  lda #$e
  sta VIDEO

  lda #$b
  sta COLORS

  _setb 1,colour
  _setb 56,wide
  _setb 1,high
  _setw $e104,dst_l
  jsr Block
  _setw $eec4,dst_l
  jsr Block

  _setb 1,wide
  _setb 56,high
  _setw $e104,dst_l
  jsr Block
  _setw $e13b,dst_l
  jsr Block

  rts




Complete:

  lda #$f
  sta VIDEO

  lda #$c
  sta COLORS

  _setb 1,colour
  _setb 56,wide
  _setb 1,high
  _setw $f104,dst_l
  jsr Block
  _setw $fec4,dst_l
  jsr Block

  _setb 1,wide
  _setb 56,high
  _setw $f104,dst_l
  jsr Block
  _setw $f13b,dst_l
  jsr Block

  rts




Controls:
  UP:
    lda INPUT
    and #KEYUP
    beq NoUP

    lda #0
    sta row
  NoUP:

  DN:
    lda INPUT
    and #KEYDN
    beq NoDN

    lda #1
    sta row
  NoDN:

  LT:
    lda INPUT
    and #KEYLT
    beq NoLT

    lda move
    beq +
    lda #$f0
    sta tomove

    jsr Movecol
+
    rts

  NoLT:

  RT:
    lda INPUT
    and #KEYRT
    beq NoRT

    lda move
    cmp #40
    bcs +
    lda #16
    sta tomove

    jsr Movecol
+
    rts

  NoRT:

  Other:
    lda INPUT
    and #%00000001
    beq NoOther

    lda #1
    sta ink

    jsr Enter

    rts

  NoOther:
    lda #2
    sta ink
    rts




Movecol:
  lda held
  cmp #8
  bcc +

  lda move
  sta last

  clc
  adc tomove
  sta move

  lda #0
  sta held
+
  inc held
  rts




Enter:
  lda held
  cmp #16
  bcc +

  jsr Verify

  lda wipe

  cmp #40
  bcs ++

  clc
  adc #8
  sta wipe

  lda #0
  sta held
+
  inc held
++
  rts





List:
  ldy static
  bne +
  lda code00,x
+
  cpy #1
  bne +
  lda code01,x
+
  cpy #2
  bne +
  lda code02,x
+
  cpy #3
  bne +
  lda code03,x
+
  cpy #4
  bne +
  lda code04,x
+
  cpy #5
  bne +
  lda code05,x
+
  cpy #6
  bne +
  lda code06,x
+
  cpy #7
  bne +
  lda code07,x
+

  rts


Vector:
  lda glyph
  asl
  tax

  lda Data,x
  sta temp
  lda Data+1,x
  sta temp+1

  jmp (temp)
Data
  dw glyph01
  dw glyph02
  dw glyph03
  dw glyph04
  dw glyph05
  dw glyph06
  dw glyph07
  dw glyph08
  rts




glyph01
  _setw block01,src_l
  rts

glyph02
  _setw block02,src_l
  rts

glyph03
  _setw block03,src_l
  rts

glyph04
  _setw block04,src_l
  rts

glyph05
  _setw block05,src_l
  rts

glyph06
  _setw block06,src_l
  rts

glyph07
  _setw block07,src_l
  rts

glyph08
  _setw block08,src_l
  rts




Verify:
  lda row
  bne bottom

  lda move

  cmp #0
  beq key01
  cmp #$10
  beq key02
  cmp #$20
  beq key03
  cmp #$30
  beq key04

bottom:
  lda move
  cmp #0
  beq key05
  cmp #$10
  beq key06
  cmp #$20
  beq key07
  cmp #$30
  beq key08

key01:
  lda #0
  jmp +
key02:
  lda #1
  jmp +
key03:
  lda #2
  jmp +
key04:
  lda #3
  jmp +
key05:
  lda #4
  jmp +
key06:
  lda #5
  jmp +
key07:
  lda #6
  jmp +
key08:
  lda #7
+
  sta key

  ldx index

  jsr List
  cmp key

  beq +

  lda #2
  sta mode

  rts
+
  inx
  stx index

  cpx count
  bcc +

  lda #3
  sta mode
+
  rts




Draw:
  ldx #0
  @yloop:
  ldy #0

@xloop:
  lda (src_l),y
  cmp mask
  beq @skip

  lda colour
  sta (dst_l),y

@skip:
  iny
  cpy wide
  bne @xloop
  _addwb src_l,wide,src_l
  _addwi dst_l,64,dst_l
  inx
  cpx high
  bne @yloop
  rts




NoSig:
  ldx #0
  @yloop:
  ldy #0

@xloop:
  lda (src_l),y
  cmp mask
  beq @skip

  lda colour
  sta (dst_l),y

@skip:
  iny
  cpy wide
  bne @xloop
  _addwb src_l,wide,src_l
  _addwi dst_l,64,dst_l
  inx
  cpx high
  bne @yloop
  rts




Block:
  ldx #0
  @yloop:
  ldy #0

@xloop:
  lda colour
  sta (dst_l),y

@skip:
  iny
  cpy wide
  bne @xloop
  _addwi dst_l,64,dst_l
  inx
  cpx high
  bne @yloop
  rts




Clear:
  ldx #0
-
  lsr
  sta $c000,x
  sta $c100,x
  sta $c200,x
  sta $c300,x
  sta $c400,x
  sta $c500,x
  sta $c600,x
  sta $c700,x
  sta $c800,x
  sta $c900,x
  sta $ca00,x
  sta $cb00,x
  sta $cc00,x
  sta $cd00,x
  sta $ce00,x
  sta $cf00,x
  dex
  bne -
rts




align $900
  hex 282222 eeeeee 9AFFA0 FE6852

align $0a00
  hex 1A491F CDF1CD FEFDFF 9AFFA0 EBFC8A FE6852 ff0000 00ff00 0000ff

align $0b00
  hex D62826 662222

align $0c00
  hex 4CD936 226622

data:
code00: hex 03 04 01 02 07
code01: hex 07 01 06 05 03
code02: hex 00 04 02 07 01
code03: hex 01 07 05 03 02
code04: hex 02 05 06 03 04
code05: hex 04 06 07 02 03
code06: hex 01 02 07 05 04
code07: hex 07 03 04 06 05

  align $1000
digit_l:
  db <(digit20)
  db <(digit19)
  db <(digit18)
  db <(digit17)
  db <(digit16)
  db <(digit15)
  db <(digit14)
  db <(digit13)
  db <(digit12)
  db <(digit11)
  db <(digit10)
  db <(digit09)
  db <(digit08)
  db <(digit07)
  db <(digit06)
  db <(digit05)
  db <(digit04)
  db <(digit03)
  db <(digit02)
  db <(digit01)
  db <(digit00)
  db <(digit00)

digit_h:
  db >(digit20)
  db >(digit19)
  db >(digit18)
  db >(digit17)
  db >(digit16)
  db >(digit15)
  db >(digit14)
  db >(digit13)
  db >(digit12)
  db >(digit11)
  db >(digit10)
  db >(digit09)
  db >(digit08)
  db >(digit07)
  db >(digit06)
  db >(digit05)
  db >(digit04)
  db >(digit03)
  db >(digit02)
  db >(digit01)
  db >(digit00)
  db >(digit00)

digit20:
  incbin "accesstuner/data/digit20.raw"
digit19:
  incbin "accesstuner/data/digit19.raw"
digit18:
  incbin "accesstuner/data/digit18.raw"
digit17:
  incbin "accesstuner/data/digit17.raw"
digit16:
  incbin "accesstuner/data/digit16.raw"
digit15:
  incbin "accesstuner/data/digit15.raw"
digit14:
  incbin "accesstuner/data/digit14.raw"
digit13:
  incbin "accesstuner/data/digit13.raw"
digit12:
  incbin "accesstuner/data/digit12.raw"
digit11:
  incbin "accesstuner/data/digit11.raw"
digit10:
  incbin "accesstuner/data/digit10.raw"
digit09:
  incbin "accesstuner/data/digit09.raw"
digit08:
  incbin "accesstuner/data/digit08.raw"
digit07:
  incbin "accesstuner/data/digit07.raw"
digit06:
  incbin "accesstuner/data/digit06.raw"
digit05:
  incbin "accesstuner/data/digit05.raw"
digit04:
  incbin "accesstuner/data/digit04.raw"
digit03:
  incbin "accesstuner/data/digit03.raw"
digit02:
  incbin "accesstuner/data/digit02.raw"
digit01:
  incbin "accesstuner/data/digit01.raw"
digit00:
  incbin "accesstuner/data/digit00.raw"

block01:
  hex 0202 0000 0000 0000
  hex 0202 0000 0000 0000
  hex 0202 0000 0000 0000
  hex 0202 0000 0000 0000
  hex 0202 0000 0000 0202
  hex 0202 0000 0000 0202
  hex 0000 0202 0000 0202
  hex 0000 0202 0000 0202
  hex 0000 0000 0202 0000
  hex 0000 0000 0202 0000
block02:
  hex 0202 0000 0202 0202
  hex 0202 0000 0202 0202
  hex 0000 0202 0000 0000
  hex 0000 0202 0000 0000
  hex 0202 0000 0000 0000
  hex 0202 0000 0000 0000
  hex 0000 0202 0202 0202
  hex 0000 0202 0202 0202
  hex 0000 0202 0202 0202
  hex 0000 0202 0202 0202
block03:
  hex 0000 0000 0202 0202
  hex 0000 0000 0202 0202
  hex 0202 0202 0000 0000
  hex 0202 0202 0000 0000
  hex 0000 0000 0202 0202
  hex 0000 0000 0202 0202
  hex 0202 0000 0202 0202
  hex 0202 0000 0202 0202
  hex 0202 0202 0000 0000
  hex 0202 0202 0000 0000
block04:
  hex 0000 0202 0202 0000
  hex 0000 0202 0202 0000
  hex 0202 0202 0000 0202
  hex 0202 0202 0000 0202
  hex 0000 0000 0000 0000
  hex 0000 0000 0000 0000
  hex 0202 0202 0000 0202
  hex 0202 0202 0000 0202
  hex 0000 0202 0202 0000
  hex 0000 0202 0202 0000
block05:
  hex 0000 0000 0202 0000
  hex 0000 0000 0202 0000
  hex 0000 0202 0000 0202
  hex 0000 0202 0000 0202
  hex 0202 0000 0202 0000
  hex 0202 0000 0202 0000
  hex 0000 0202 0000 0000
  hex 0000 0202 0000 0000
  hex 0202 0000 0202 0000
  hex 0202 0000 0202 0000
block06:
  hex 0000 0202 0202 0000
  hex 0000 0202 0202 0000
  hex 0000 0202 0000 0000
  hex 0000 0202 0000 0000
  hex 0000 0202 0000 0000
  hex 0000 0202 0000 0000
  hex 0000 0202 0202 0202
  hex 0000 0202 0202 0202
  hex 0202 0202 0000 0000
  hex 0202 0202 0000 0000
block07:
  hex 0202 0202 0202 0202
  hex 0202 0202 0202 0202
  hex 0000 0000 0000 0202
  hex 0000 0000 0000 0202
  hex 0202 0202 0000 0202
  hex 0202 0202 0000 0202
  hex 0000 0000 0000 0202
  hex 0000 0000 0000 0202
  hex 0202 0202 0202 0000
  hex 0202 0202 0202 0000
block08:
  hex 0202 0202 0202 0202
  hex 0202 0202 0202 0202
  hex 0000 0202 0202 0000
  hex 0000 0202 0202 0000
  hex 0000 0000 0000 0000
  hex 0000 0000 0000 0000
  hex 0000 0000 0000 0000
  hex 0000 0000 0000 0000
  hex 0202 0202 0202 0202
  hex 0202 0202 0202 0202
box:
  hex 04 0404 0404 0404 0404 0404 0404 04
  hex 04 0404 0404 0404 0404 0404 0404 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0400 0000 0000 0000 0000 0004 04
  hex 04 0404 0404 0404 0404 0404 0404 04
  hex 04 0404 0404 0404 0404 0404 0404 04

static_l:
  db <(static00)
  db <(static01)
  db <(static02)
  db <(static03)
  db <(static04)
  db <(static05)
  db <(static06)
  db <(static07)
static_h:
  db >(static00)
  db >(static01)
  db >(static02)
  db >(static03)
  db >(static04)
  db >(static05)
  db >(static06)
  db >(static07)

signal:
  incbin "accesstuner/data/signal.raw"

  align $2000
static00:
  incbin "accesstuner/data/static00.raw"
static01:
  incbin "accesstuner/data/static01.raw"
static02:
  incbin "accesstuner/data/static02.raw"
static03:
  incbin "accesstuner/data/static03.raw"
static04:
  incbin "accesstuner/data/static04.raw"
static05:
  incbin "accesstuner/data/static05.raw"
static06:
  incbin "accesstuner/data/static06.raw"
static07:
  incbin "accesstuner/data/static07.raw"

  align $d000
  incbin "accesstuner/data/ui.raw"

  align $e780
  incbin "accesstuner/data/error.raw"

  align $f6c0
  incbin "accesstuner/data/accept.raw"
